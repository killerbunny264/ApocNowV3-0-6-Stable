﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class GSDifficulty
{
	public static float[] GameDifficultyBonus = new float[6]
	{
		1.0f,
		1.2f,
		1.5f,
		1.7f,
		2.0f,
		2.5f
	};
  
  public class GSDifficulty_Init : IModApi
  {
    public void InitMod(Mod _modInstance)
    {
        Debug.Log(" Loading Patch: " + GetType().ToString());
        var harmony = new Harmony(GetType().ToString());
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }
  }
  
  [HarmonyPatch(typeof(GameStagesFromXml), "Load")]
  public class Patch_GameStagesFromXml_Load
  {
    static bool Prefix(XmlFile _xmlFile)
    {
      XmlElement documentElement = _xmlFile.XmlDoc.DocumentElement;
      if (documentElement.ChildNodes.Count < 1)
      {
        throw new Exception("Missing root element!");
      }
      foreach (object childNode in documentElement.ChildNodes)
      {
        XmlElement xmlElement = childNode as XmlElement;
        if (xmlElement != null)
        {
          if (xmlElement.Name == "config")
          {
            if (xmlElement.HasAttribute("scavengerGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[0] = StringParsers.ParseFloat(xmlElement.GetAttribute("scavengerGameDifficultyBonus"));
            }
            if (xmlElement.HasAttribute("adventurerGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[1] = StringParsers.ParseFloat(xmlElement.GetAttribute("adventurerGameDifficultyBonus"));
            }
            if (xmlElement.HasAttribute("nomadGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[2] = StringParsers.ParseFloat(xmlElement.GetAttribute("nomadGameDifficultyBonus"));
            }
            if (xmlElement.HasAttribute("warriorGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[3] = StringParsers.ParseFloat(xmlElement.GetAttribute("warriorGameDifficultyBonus"));
            }
            if (xmlElement.HasAttribute("survivalistGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[4] = StringParsers.ParseFloat(xmlElement.GetAttribute("survivalistGameDifficultyBonus"));
            }
            if (xmlElement.HasAttribute("insaneGameDifficultyBonus"))
            {
              GSDifficulty.GameDifficultyBonus[5] = StringParsers.ParseFloat(xmlElement.GetAttribute("insaneGameDifficultyBonus"));
            }
          }
        }
      }
      
      return true;
    }
  }
  
  [HarmonyPatch(typeof(GameModeAbstract), "Init")]
  public class Patch_GameModeAbstract_Init
  {
    static void Postfix()
    {
      GameStats.Set(EnumGameStats.GameDifficultyBonus, GSDifficulty.GameDifficultyBonus[GamePrefs.GetInt(EnumGamePrefs.GameDifficulty)]);
    }
  }
}
