﻿Key,english
ApocNowVerySmallGiftBox,Very Small Gift Box
ApocNowSmallGiftBox,Small Gift Box
ApocNowMediumGiftBox,Medium Gift Box
ApocNowLargeGiftBox,Large Gift Box
ApocNowSuperLargeGiftBox,Super Large Gift Box
ApocNowVerySmallGiftBoxLocked,"Very Small Gift Box(Locked)"
ApocNowSmallGiftBoxLocked,"Small Gift Box(Locked)"
ApocNowMediumGiftBoxLocked,"Medium Gift Box(Locked)"
ApocNowLargeGiftBoxLocked,"Large Gift Box(Locked)"
ApocNowSuperLargeGiftBoxLocked,"Super Large Gift Box(Locked)"
ChristmasGiftDesc,A Nicely Wrapped Christmas Present.\nYou Might be Super Lucky \nOr you might be Super Unlucky!.\n\nSo Try Your Luck. 
ApocNowChristmasCookies1,Сookie with raisins
ApocNowChristmasCookies2,Double cookie
ApocNowChristmasCookies3,Сhocolate cookie
ApocNowChristmasCookies4,Cookie bear
ApocNowChristmasCookies5,Cookie man
ApocNowChristmasCookies6,Cookie tree
ApocNowChristmasCookies7,Ginger bread cookie
ApocNowChristmasCookiesDesc,It's Tasty And Fills you up a tiny bit